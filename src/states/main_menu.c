/* main_menu.c */

#include "main_menu.h"
#include "options.h"
#include "running.h"
#include "../constants.h"
#include "../main.h"
#include "../mm.h"
#include "../audio.h"
#include "../settings.h"
#include "../lib/easings.h"
#include <string.h>
#include <raylib.h>


// Initializers for the flash colors (if we use raylib's defines we cannot make a const array because they define compound literals)
#define INI_BLACK .r = 0, .g = 0, .b = 0, .a = 255
#define INI_RED .r = 255, .g = 0, .b = 0, .a = 255
#define INI_GREEN .r = 0, .g = 255, .b = 0, .a = 255


enum main_menu_states {
	STATE_BOOT,
	STATE_TITLE,
};

enum letters {
	LT_S,
	LT_N,
	LT_A,
	LT_C,
	LT_K,
	NUM_LETTERS,
};

static const Color flash[] = {{INI_BLACK}, {INI_GREEN}, {INI_BLACK}, {INI_RED}};

#define SUBTITLE "BITMAPPED VECTORPENT"

#define LETTER_HEIGHT 64.0f
#define LETTER_WIDTH 80.0f
#define LH_WITH_PAD ((LETTER_HEIGHT) + 8.0f)
#define INTRO_DURATION 120.0f
#define TIME_SEPARATION 5.0f  // Time offset between letters
#define ANIMATION_DURATION ((INTRO_DURATION) - ((TIME_SEPARATION) * ((NUM_LETTERS) * 2.0f)))
#define STARTING_Y ((S_V_CELLS) * (CELLSIZE))
#define FINAL_Y (1.0f * (CELLSIZE))
#define ARG_C ((FINAL_Y) - (STARTING_Y))

static float intro_t = 0.0f;
static struct {
	const float x;
	float y;
	const float rec_top_y;
	float t;
} letters[NUM_LETTERS] = {
	[LT_S] = {.x = 0.0f * CELLSIZE, .y = STARTING_Y, .rec_top_y = 0.0f},
	[LT_N] = {.x = 6.0f * CELLSIZE, .y = STARTING_Y, .rec_top_y = (LT_N * LH_WITH_PAD)},
	[LT_A] = {.x = 13.0f * CELLSIZE, .y = STARTING_Y, .rec_top_y = (LT_A * LH_WITH_PAD)},
	[LT_C] = {.x = 20.0f * CELLSIZE, .y = STARTING_Y, .rec_top_y = (LT_C * LH_WITH_PAD)},
	[LT_K] = {.x = 26.0f * CELLSIZE, .y = STARTING_Y, .rec_top_y = (LT_K * LH_WITH_PAD)},
};


static void change_main_menu_state(enum main_menu_states new_mmstate);
static void boot_entry(void);
static void boot_update(void);
static void boot_render(void);
static void boot_exit_action(void);
static void main_menu_entry(void);
static void main_menu_update(void);
static void main_menu_render(void);
static void animate_subtitle(void);
static void display_title(void);
static void display_high_score(const int x, const int y);


void main_menu(void)
{
	change_main_menu_state(STATE_BOOT);
}

static void change_main_menu_state(enum main_menu_states new_mmstate)
{
	switch (new_mmstate) {
	case STATE_BOOT:
		transition_substate(
			boot_entry,
			boot_update,
			boot_render,
			boot_exit_action
		);

		break;

	case STATE_TITLE:
		transition_substate(
			main_menu_entry,
			main_menu_update,
			main_menu_render,
			empty_exit_action
		);

		break;
	}
}

static void boot_entry(void)
{
	if (audio_is_music_playing() == false) {
		audio_play_song(SONG_TITLE_SCREEN);
	}

	letters[0].t = 0.0f;
	for (int i = 1; i < NUM_LETTERS; ++i) {
		letters[i].t = letters[i - 1].t - (TIME_SEPARATION * i);
	}
}

static void boot_update(void)
{
	for (int i = 0; i < NUM_LETTERS; ++i) {
		if (letters[i].t >= ANIMATION_DURATION) {
			continue;
		}

		letters[i].t += 1.0f;
		letters[i].y = EaseBackOut(letters[i].t, STARTING_Y, ARG_C,
		                           ANIMATION_DURATION);
	}

	intro_t += 1.0f;

	if (IsKeyPressed(KEY_OK) || IsKeyPressed(KEY_GOBACK) ||
	    intro_t >= INTRO_DURATION)
	{
		change_main_menu_state(STATE_TITLE);

		return;
	}
}

static void boot_render(void)
{
	static const float flash_qty = sizeof(flash) / sizeof(flash[0]);

	if (intro_t > INTRO_DURATION - flash_qty) {
		const int i = INTRO_DURATION - intro_t;

		ClearBackground(flash[i]);
	}

	animate_subtitle();
	display_title();
}

static void boot_exit_action(void)
{
	intro_t = 0.0f;
}

static void main_menu_entry(void)
{
	audio_play_song(SONG_TITLE_SCREEN);

	for (int i = 0; i < NUM_LETTERS; ++i) {
		letters[i].y = FINAL_Y;
	}
}

static void main_menu_update(void)
{
	if (IsKeyPressed(KEY_OK)) {
		game_running();
	}
	else
	if (IsKeyPressed(KEY_GOBACK)) {
		PlaySound(sounds[SOUND_MENU_ENTER]);
		options_menu();
	}
	else
	if (IsKeyPressed(KEY_Q)) {
		exit_game();
	}
}

#define SUBTITLE_X 6.0f
#define SUBTITLE_Y 10.0f
#define HSCORE_X 1
#define HSCORE_Y 13
static void main_menu_render(void)
{
	display_title();
	DrawTextEx(font, SUBTITLE,
	           (Vector2){SUBTITLE_X * CELLSIZE, SUBTITLE_Y * CELLSIZE},
	           FONTSIZE, 0.0f, WHITE);
	DrawTextEx(font, TextFormat("ENTER -> Play   BCKSP -> Options"),
	           (Vector2){0.0f, V_CELLS * CELLSIZE}, FONTSIZE, 0.0f, WHITE);
	display_high_score(HSCORE_X, HSCORE_Y);
}

static void display_title(void)
{
	for (int i = 0; i < NUM_LETTERS; ++i) {
		DrawTextureRec(title_tex,
		               (Rectangle){.x = 0.0f, .y = letters[i].rec_top_y,
		                           .width = LETTER_WIDTH,
		                           .height = LETTER_HEIGHT},
		               (Vector2){.x = letters[i].x, .y = letters[i].y}, WHITE);
	}
}

#define SUBTITLE_X 6.0f
#define SUBTITLE_Y 10.0f
static void animate_subtitle(void)
{
	int shown_letters = (intro_t / INTRO_DURATION) * strlen(SUBTITLE);

	DrawTextEx(font, TextFormat("%.*s", shown_letters, SUBTITLE),
	           (Vector2){SUBTITLE_X * CELLSIZE, SUBTITLE_Y * CELLSIZE},
	           FONTSIZE, 0.0f, WHITE);
}

//#define SCORE_FIRST
static void display_high_score(const int x, const int y)
{
	struct score hs_open;
	struct score hs_wall;

	const float xf = x * CELLSIZE;
	float yf = y * CELLSIZE - FONTSIZE;

	DrawTextEx(font, TextFormat("         HIGH SCORES!"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	yf += FONTSIZE;
	DrawTextEx(font, TextFormat("RANK     OPEN        WALLED"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
#ifdef SCORE_FIRST
	hs_open = stt_get_high_score(HARD, false);
	hs_wall = stt_get_high_score(HARD, true);
	DrawTextEx(font, TextFormat("HARD %8d %3s %8d %3s",
	                            hs_open.score, hs_open.name,
	                            hs_wall.score, hs_wall.name),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	hs_open = stt_get_high_score(MEDIUM, false);
	hs_wall = stt_get_high_score(MEDIUM, true);
	DrawTextEx(font, TextFormat("MED  %8d %3s %8d %3s",
	                            hs_open.score, hs_open.name,
	                            hs_wall.score, hs_wall.name),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	hs_open = stt_get_high_score(EASY, false);
	hs_wall = stt_get_high_score(EASY, true);
	DrawTextEx(font, TextFormat("EASY %8d %3s %8d %3s",
	                            hs_open.score, hs_open.name,
	                            hs_wall.score, hs_wall.name),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
#else
	hs_open = stt_get_high_score(HARD, false);
	hs_wall = stt_get_high_score(HARD, true);
	DrawTextEx(font, TextFormat("HARD %3s %8d %3s %8d",
	                            hs_open.name, hs_open.score,
	                            hs_wall.name, hs_wall.score),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	hs_open = stt_get_high_score(MEDIUM, false);
	hs_wall = stt_get_high_score(MEDIUM, true);
	DrawTextEx(font, TextFormat("MED  %3s %8d %3s %8d",
	                            hs_open.name, hs_open.score,
	                            hs_wall.name, hs_wall.score),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	hs_open = stt_get_high_score(EASY, false);
	hs_wall = stt_get_high_score(EASY, true);
	DrawTextEx(font, TextFormat("EASY %3s %8d %3s %8d",
	                            hs_open.name, hs_open.score,
	                            hs_wall.name, hs_wall.score),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
#endif
	yf += FONTSIZE;
	hs_open = stt_get_highest_score();
	DrawTextEx(font, TextFormat("  HIGHEST SCORE %3s %-8d",
	                            hs_open.name, hs_open.score),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
}