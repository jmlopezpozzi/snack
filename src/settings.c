/* settings.c */

#include "settings.h"
#include "constants.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <raylib.h>

#define SCORE_FILENAME "score.dat"
#define WALL_OPTIONS 2  // 0, walls off; 1, walls on


static enum rank cur_rank = NUM_RANKS / 2;
static bool walls = false;

static struct score high_scores[NUM_RANKS][WALL_OPTIONS];
static struct score highest_score;

static const size_t record_size = sizeof(char) * NAME_SIZE + sizeof(int);


static void write_score(FILE *fp, enum rank difficulty, bool walls_option,
                        int score, const char *name);
static int read_score_file(FILE *fp, struct score *hscore_table, const int cols,
                           const int rows);


void stt_init_score(void)
{
	memset(&high_scores, 0, sizeof(high_scores));
	memset(&highest_score, 0, sizeof(highest_score));

	FILE *score_file = fopen(SCORE_FILENAME, "rb");

	if (score_file == NULL) {
		// Could not open score_file
		score_file = fopen(SCORE_FILENAME, "wb+");

		if (score_file == NULL) {
			// Could not create score_file
			TraceLog(LOG_WARNING,
			         TextFormat("%s: Could not open nor create score file",
			                    __func__));

			return;
		}
		else {
			// Initialize score_file to 0s
			for (int j = 0; j < NUM_RANKS; ++j) {
				for (int i = 0; i < WALL_OPTIONS; ++i) {
					write_score(score_file, j, i, 0, "");
				}
			}

			if (ferror(score_file) != 0) {
				TraceLog(LOG_WARNING,
				         TextFormat("%s: Error when writing to score file",
				                    __func__));
				fclose(score_file);

				return;
			}
		}
	}

	// Fill high_scores table
	if (read_score_file(score_file, high_scores[0], WALL_OPTIONS, NUM_RANKS)
	    != 0)
	{
		TraceLog(LOG_WARNING, TextFormat("%s: Error reading score file",
		                                 __func__));
		fclose(score_file);
		memset(&high_scores, 0, sizeof(high_scores));
		memset(&highest_score, 0, sizeof(highest_score));

		return;
	}

	for (int j = 0; j < NUM_RANKS; ++j) {
		for (int i = 0; i < WALL_OPTIONS; ++i) {
			if (highest_score.score <= high_scores[j][i].score) {
				highest_score = high_scores[j][i];
			}
		}
	}

	fclose(score_file);
}

void stt_reset_score(void)
{
	memset(&high_scores, 0, sizeof(high_scores));
	memset(&highest_score, 0, sizeof(highest_score));

	FILE *score_file = fopen(SCORE_FILENAME, "rb+");

	if (score_file == NULL) {
		// Could not open score file
		score_file = fopen(SCORE_FILENAME, "wb");

		if (score_file == NULL) {
			// Could not create score file
			TraceLog(LOG_WARNING,
			         TextFormat("%s: Could not open nor create score file",
			                    __func__));

			return;
		}
	}

	for (int j = 0; j < NUM_RANKS; ++j) {
		for (int i = 0; i < WALL_OPTIONS; ++i) {
			write_score(score_file, j, i, 0, "");
		}
	}

	if (ferror(score_file) != 0) {
		TraceLog(LOG_WARNING, TextFormat("%s: Error when writing to score file",
		                                 __func__));
	}

	fclose(score_file);
}

struct score stt_get_high_score(enum rank difficulty, bool walls_option)
{
	return high_scores[difficulty][walls_option];
}

struct score stt_get_mode_hi_score(void)
{
	return high_scores[cur_rank][walls];
}

struct score stt_get_highest_score(void)
{
	return highest_score;
}

void stt_set_mode_hi_score(int score, const char *name)
{
	struct score *mode_hscore = &high_scores[cur_rank][walls];

	// Data passed is supposed to be the high score for current mode
	mode_hscore->score = score;
	strncpy(mode_hscore->name, name, NAME_SIZE - 1);
	mode_hscore->name[NAME_SIZE - 1] = '\0';

	if (mode_hscore->score > highest_score.score) {  // NOTE: In case both are equal, toughest mode is not prevailing...
		highest_score = *mode_hscore;
	}

	FILE *score_file = fopen(SCORE_FILENAME, "rb+");

	if (score_file == NULL) {
		// Could not open score file
		TraceLog(LOG_WARNING, TextFormat("%s: Could not open score file",
		                                 __func__));

		return;
	}

	write_score(score_file, cur_rank, walls, score, name);

	if (ferror(score_file) != 0) {
		TraceLog(LOG_WARNING, TextFormat("%s: Error when writing to score file",
		                                 __func__));
	}

	fclose(score_file);
}

static void write_score(FILE *fp, enum rank difficulty, bool walls_option,
                        int score, const char *name)
{
	char name_buf[NAME_SIZE];

	strncpy(name_buf, name, NAME_SIZE - 1);
	name_buf[NAME_SIZE - 1] = '\0';

	fseek(fp, (long) (record_size * difficulty * WALL_OPTIONS +
	                  record_size * walls_option),
	      SEEK_SET);
	fwrite(name_buf, sizeof(name_buf), 1, fp);
	fwrite(&score, sizeof(score), 1, fp);
}

static int read_score_file(FILE *fp, struct score *hscore_table, const int cols,
                           const int rows)
{
	int score;
	char name[NAME_SIZE];

	rewind(fp);

	for (int j = 0; j < rows; ++j) {
		for (int i = 0; i < cols; ++i) {
			if (fread(name, sizeof(name), 1, fp) != 1) {
				// Error
				return 1;
			}
			if (fread(&score, sizeof(score), 1, fp) != 1) {
				// Error
				return 2;
			}

			struct score sc;

			sc.score = score;
			name[NAME_SIZE - 1] = '\0';
			strncpy(sc.name, name, NAME_SIZE);

			hscore_table[j * cols + i] = sc;
		}
	}

	return 0;
}

enum rank stt_get_rank(void)
{
	return cur_rank;
}

void stt_set_rank(enum rank new_rank)
{
	if (new_rank < 0 || new_rank >= NUM_RANKS) {
		return;
	}

	cur_rank = new_rank;
}

bool stt_walls_are_active(void)
{
	return walls;
}

void stt_set_walls_option_to(bool toggle)
{
	walls = toggle;
}