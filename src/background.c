/* background.c */

#include "background.h"
#include "lib/rmistify.h"
#include <stdbool.h>
#include <raylib.h>  // GetRandomValue


#include "backgrounds.h"  // Contains the backround definitions and defines NUM_BACKGROUNDS

const int num_backgrounds = NUM_BACKGROUNDS;  // To be shared outside the module (NOTE: quantity of backgrounds must be lower than INT_MAX)

static struct {
	bool option_active;
	int selected;
	int loaded;
	struct rmt_figure *figures[2];
} background = {

	.option_active = true,
	.selected = NUM_BACKGROUNDS,
	.loaded = -1,
};


/* If background.selected is outside range chooses a random background */
void background_select(int bg_id)
{
	background.selected = bg_id;
}

int background_selected(void)
{
	return background.selected;
}

int background_loaded(void)
{
	return background.loaded;
}

bool background_option_active(void)
{
	return background.option_active;
}

void background_option_set_to(bool toggle)
{
	background.option_active = toggle;
}

bool background_selection_is_random(void)
{
	return (background.selected < 0 || background.selected >= num_backgrounds);
}

const char *background_get_name(int bg_id)
{
	return backgrounds[bg_id].name;
}

int background_set(void)
{
	if (background.selected == background.loaded) {
		return 0;
	}
	else
	if (background.loaded >= 0) {
		background_stop();
	}

	int bg_id;

	if (background_selection_is_random()) {
		bg_id = GetRandomValue(0, NUM_BACKGROUNDS - 1);
	}
	else {
		bg_id = background_selected();
	}

	int num_figures = backgrounds[bg_id].num_figures;

	for (int i = 0; i < num_figures; ++i) {
		background.figures[i] = rmt_create(&backgrounds[bg_id].args[i]);
	}

	background.loaded = bg_id;

	return 0;
}

int background_stop(void)
{
	if (background.loaded < 0) {
		return 1;
	}

	int num_figures = backgrounds[background.loaded].num_figures;

	for (int i = 0; i < num_figures; ++i) {
		background.figures[i] = rmt_destroy(background.figures[i]);
	}

	background.loaded = -1;

	return 0;
}

int background_change(int bg_id)
{
	background_select(bg_id);
	return background_set();
}

void background_update(void)
{
	if (background.option_active == false) {
		return;
	}
	if (background.loaded < 0) {
		return;
	}

	const int num_figures = backgrounds[background.loaded].num_figures;

	for (int i = 0; i < num_figures; ++i) {
		rmt_update(background.figures[i]);
	}
}

void background_update_colorcycle(void)
{
	if (background.option_active == false) {
		return;
	}
	if (background.loaded < 0) {
		return;
	}

	const int num_figures = backgrounds[background.loaded].num_figures;

	for (int i = 0; i < num_figures; ++i) {
		rmt_update_colorcycle(background.figures[i]);
	}
}

void background_render(void)
{
	if (background.option_active == false) {
		return;
	}
	if (background.loaded < 0) {
		return;
	}

	const int num_figures = backgrounds[background.loaded].num_figures;

	for (int i = 0; i < num_figures; ++i) {
		rmt_render(background.figures[i]);
	}
}