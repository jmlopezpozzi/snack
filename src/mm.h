/* mm.h */

#ifndef MM_H
#define MM_H


extern long long cur_frame;  // Debug


int run_loop(void (*initial_state)(void));
int start_framebuffer(int width, int height);
int stop_framebuffer(void);
void transition_substate(void (*new_entry_action)(void),
                         void (*new_update)(void),
                         void (*new_render_func)(void),
                         void (*new_exit_action)(void));
void push_entry_action(void (*new_entry_action)(void));
void push_exit_action(void (*new_exit_action)(void));
void exit_game(void);
void empty_exit_action(void);
void empty_entry_action(void);


#endif