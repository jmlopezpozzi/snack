/* video.c */

#include "video.h"
#include "constants.h"
#include "main.h"  // load_graphics(), unload_graphics(), window_title
#include "mm.h"
#include <stddef.h>
#include <raylib.h>
#include <stdio.h>  // Debug

#define DFT_VIDEO_SCALE 2
#define DFT_VIDEO_SCALE_MAX 4


static struct {
	int scale;
	int scale_windowed;
	int scale_max;
	bool fullscreen;
	bool vsync;
	int monitor_width;
	int monitor_height;
	Vector2 origin;
} video = {
	.scale = DFT_VIDEO_SCALE,
	.scale_windowed = DFT_VIDEO_SCALE,
	.scale_max = DFT_VIDEO_SCALE_MAX,
	.fullscreen = false,
	.vsync = false,
	.origin = {.x = 0.0f, .y = 0.0f},
};


const int * const video_scale = &video.scale;
const Vector2 * const video_origin = &video.origin;
const bool * const video_vsync = &video.vsync;

void (*video_change_callback)(void);

static void video_info_update(void)
{
	video.monitor_width = GetMonitorWidth(0);
	video.monitor_height = GetMonitorHeight(0);

	int maxscale_x = video.monitor_width / (S_H_CELLS * CELLSIZE);
	int maxscale_y = video.monitor_height / (S_V_CELLS * CELLSIZE);

	if (maxscale_x < maxscale_y) {
		video.scale_max = maxscale_x;
	}
	else {
		video.scale_max = maxscale_y;
	}

	if (video.scale_max < 1) {
		// There was an error gathering monitor data
		video.scale_max = DFT_VIDEO_SCALE_MAX;
	}

	printf("monitor_width=%d\n", video.monitor_width);
	printf("monitor_height=%d\n", video.monitor_height);
	printf("maxscale_x=%d\n", maxscale_x);
	printf("maxscale_y=%d\n", maxscale_y);
	printf("scale_max=%d\n", video.scale_max);
}

void video_info_init(void)
{
	video_info_update();

	video.scale = video.scale_max - 1;
	video.scale_windowed = video.scale_max - 1;

	if (video_change_callback != NULL) {
		(*video_change_callback)();
	}
}

int video_set_scale(int scale)
{
	video_info_update();

	if (scale < 1 || scale > video.scale_max || video.fullscreen == true) {
		return video.scale;
	}

	video.scale_windowed = scale;
	video.scale = scale;

	int total_x = S_H_CELLS * CELLSIZE_I * scale;
	int total_y = S_V_CELLS * CELLSIZE_I * scale;

	SetWindowSize(total_x, total_y);

	total_x = (video.monitor_width / 2) - (total_x / 2);
	total_y = (video.monitor_height / 2) - (total_y / 2);

	SetWindowPosition(total_x, total_y);

	if (video_change_callback != NULL) {
		(*video_change_callback)();
	}

	return scale;
}

// FSCREEN_MODE:
//   Undefined: Changes video mode to set fullscreen mode. The monitor video mode change could be avoided but for some reason using SetWindowSize before using ToggleFullscreen does not work
//   Value 0: Closes and opens the window in fullscreen mode. In order for this to work graphics have to be reloaded. The monitor does not switch video mode
//   Value 1: Closes and opens the window maximized and undecorated. Graphics have to be reloaded and monitor does not change video mode. On some desktops, some elements can still appear in front of the window though.
//#define FSCREEN_MODE 0

void video_toggle_fullscreen(void)
{
	video_info_update();

	if (video.monitor_width < 1 || video.monitor_height < 1) {
		return;
	}

	video.fullscreen = !video.fullscreen;

#ifndef FSCREEN_MODE
	if (video.fullscreen == true) {
		video.scale = video.scale_max;
		video.origin.x = -(video.monitor_width - S_H_CELLS * CELLSIZE * video.scale) / 2.0f;
		video.origin.y = -(video.monitor_height - S_V_CELLS * CELLSIZE * video.scale) / 2.0f;
		//SetWindowSize(video.monitor_width, video.monitor_height);  // Does not work properly here, has to go after ToggleFullscreen
		ToggleFullscreen();
		SetWindowSize(video.monitor_width, video.monitor_height);
		HideCursor();

		/*printf("video.scale=%d\n", video.scale);
		printf("video.monitor_width=%d\n", video.monitor_width);
		printf("playfield scale=%f\n", S_H_CELLS * CELLSIZE * video.scale);
		printf("video.origin.x=%f\n", video.origin.x);*/
	}
	else {
		ToggleFullscreen();
		video.origin.x = 0.0f;
		video.origin.y = 0.0f;
		ShowCursor();
		video_set_scale(video.scale_windowed);
	}

#elif FSCREEN_MODE == 0
	if (video.fullscreen == true) {
		video.scale = video.scale_max;
		video.origin.x = -(video.monitor_width - S_H_CELLS * CELLSIZE * video.scale) / 2.0f;
		video.origin.y = -(video.monitor_height - S_V_CELLS * CELLSIZE * video.scale) / 2.0f;

		stop_framebuffer();
		unload_graphics();
		CloseWindow();

		ToggleFullscreen();
		InitWindow(video.monitor_width, video.monitor_height, window_title);
		HideCursor();
		SetTargetFPS(DFT_FPS);
		load_graphics();
		start_framebuffer(S_H_CELLS * CELLSIZE_I, S_V_CELLS * CELLSIZE_I);
	}
	else {
		video.origin.x = 0.0f;
		video.origin.y = 0.0f;

		stop_framebuffer();
		unload_graphics();
		CloseWindow();

		ToggleFullscreen();
		InitWindow(10, 10, window_title);
		ShowCursor();
		video_set_scale(video.scale_windowed);
		SetTargetFPS(DFT_FPS);
		load_graphics();
		start_framebuffer(S_H_CELLS * CELLSIZE_I, S_V_CELLS * CELLSIZE_I);
	}

#elif FSCREEN_MODE == 1
	if (video.fullscreen == true) {
		video.scale = video.scale_max;
		video.origin.x = -(video.monitor_width - S_H_CELLS * CELLSIZE * video.scale) / 2.0f;
		video.origin.y = -(video.monitor_height - S_V_CELLS * CELLSIZE * video.scale) / 2.0f;

		stop_framebuffer();
		unload_graphics();
		CloseWindow();

		SetConfigFlags(FLAG_WINDOW_UNDECORATED | (FLAG_VSYNC_HINT * video.vsync));
		InitWindow(video.monitor_width, video.monitor_height, window_title);
		HideCursor();
		SetTargetFPS(DFT_FPS);
		load_graphics();
		start_framebuffer(S_H_CELLS * CELLSIZE_I, S_V_CELLS * CELLSIZE_I);
	}
	else {
		video.origin.x = 0.0f;
		video.origin.y = 0.0f;

		stop_framebuffer();
		unload_graphics();
		CloseWindow();

		SetConfigFlags(FLAG_VSYNC_HINT * video.vsync);
		InitWindow(10, 10, window_title);
		ShowCursor();
		video_set_scale(video.scale_windowed);
		SetTargetFPS(DFT_FPS);
		load_graphics();
		start_framebuffer(S_H_CELLS * CELLSIZE_I, S_V_CELLS * CELLSIZE_I);
	}

#endif

	if (video_change_callback != NULL) {
		(*video_change_callback)();
	}
}

void video_toggle_vsync(void)
{
	video_info_update();

#if FSCREEN_MODE == 2
	ConfigFlag cf = FLAG_WINDOW_UNDECORATED * video.fullscreen;
#else
	ConfigFlag cf = 0;
#endif

	video.vsync = !video.vsync;

	if (video.vsync == true) {
		cf |= FLAG_VSYNC_HINT;
	}

	stop_framebuffer();
	unload_graphics();
	CloseWindow();

	SetConfigFlags(cf);

	if (video.fullscreen == true) {
		ToggleFullscreen();
		InitWindow(video.monitor_width, video.monitor_height, window_title);
		ToggleFullscreen();
		HideCursor();
	}
	else {
		InitWindow(S_H_CELLS * CELLSIZE_I * *video_scale,
		           S_V_CELLS * CELLSIZE_I * *video_scale, window_title);
	}

	SetTargetFPS(DFT_FPS);
	load_graphics();
	start_framebuffer(S_H_CELLS * CELLSIZE_I, S_V_CELLS * CELLSIZE_I);

	if (video_change_callback != NULL) {
		(*video_change_callback)();
	}
}