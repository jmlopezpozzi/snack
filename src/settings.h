/* settings.h */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <stdbool.h>


enum rank {EASY = 0, MEDIUM, HARD, NUM_RANKS};

#define NAME_SIZE 4  // 3 initials and the null-char

struct score {
	char name[NAME_SIZE];
	int score;
};


void stt_init_score(void);
void stt_reset_score(void);
struct score stt_get_high_score(enum rank difficulty, bool walls_option);
struct score stt_get_mode_hi_score(void);
struct score stt_get_highest_score(void);
void stt_set_mode_hi_score(int score, const char *name);
enum rank stt_get_rank(void);
void stt_set_rank(enum rank new_rank);
bool stt_walls_are_active(void);
void stt_set_walls_option_to(bool toggle);


#endif