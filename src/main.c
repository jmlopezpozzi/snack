/* main.c */

#include "main.h"
#include "constants.h"
#include "video.h"
#include "mm.h"
#include "audio.h"
#include "settings.h"
#include "background.h"
#include "states/main_menu.h"
#include <raylib.h>


#define TITLE_TEXTURE_FILENAME "title_vert_pad2.png"
#define SNAKE_TEXTURE_FILENAME "snack_h3.png"
#define FONT_FILENAME "ZXfont.png"


Texture2D title_tex;
Texture2D snake_tex;
Font font;

const char * const window_title = "snack: bitmapped vectorpent";

static void init(void);
static void close(void);


int main(void)
{
	init();
	run_loop(main_menu);
	close();
}

static void init(void)
{
	SetConfigFlags(FLAG_WINDOW_HIDDEN);
	InitWindow(50, 10, "setting up");
	video_info_init();
	CloseWindow();
	SetConfigFlags(0);
	InitWindow(S_H_CELLS * CELLSIZE_I * *video_scale,
	           S_V_CELLS * CELLSIZE_I * *video_scale, window_title);
	SetTargetFPS(DFT_FPS);
	load_graphics();
	start_framebuffer(S_H_CELLS * CELLSIZE_I, S_V_CELLS * CELLSIZE_I);
	audio_init();
	background_set();
	stt_init_score();
}

void load_graphics(void)
{
	Image ifont;

	title_tex = LoadTexture(TITLE_TEXTURE_FILENAME);
	snake_tex = LoadTexture(SNAKE_TEXTURE_FILENAME);
	ifont = LoadImage(FONT_FILENAME);
	font = LoadFontFromImage(ifont, MAGENTA, ' ');
	UnloadImage(ifont);
}

void unload_graphics(void)
{
	UnloadFont(font);
	UnloadTexture(snake_tex);
	UnloadTexture(title_tex);
}

static void close(void)
{
	background_stop();
	audio_close();
	stop_framebuffer();
	unload_graphics();
	CloseWindow();
}