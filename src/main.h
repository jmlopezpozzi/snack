/* main.h */

#ifndef MAIN_H
#define MAIN_H

#include <raylib.h>


extern Texture2D title_tex;
extern Texture2D snake_tex;
extern Font font;

extern const char * const window_title;

void load_graphics(void);
void unload_graphics(void);

#endif